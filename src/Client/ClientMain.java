import java.net.Socket;
import java.net.UnknownHostException;
import java.io.IOException;
import java.io.DataOutputStream;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Main class for client side of the calculator
 * @author Dimitris Lygizos
 */
public class ClientMain
{
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        Logger logger = Logger.getLogger("CalculatorClient");
        logger.setLevel(Level.INFO);
        int x,y, port;
        String c, hostname, action = InputUtils.menuInput();                 // let the user choose what to do

        if(args.length != 2) {      // command line arguments sanity check
            logger.warning("Syntax error invalid arguments: you must provide exactly 2 arguments,\nServer hostname and listening port");
            return;
        }

        hostname = args[0];                 // get hostname from command line
        port = Integer.parseInt(args[1]);   // get Server listener port from command line
        try {
            Socket clientSocket = new Socket(hostname, port);   // create the new socket
            // clientSocket.connect(null);
            while(!action.equalsIgnoreCase("exit"))
            {
                if(action.equalsIgnoreCase("domath"))           // condition to perform calculation
                {
                    try {
                        System.out.print("Type the First Number\n>> ");
                        x = InputUtils.getNumber();
                        System.out.print("\nType the arithmetic operator\n>> ");
                        c = InputUtils.getHostname();
                        if( c == null ) {
                            logger.warning("Empty operator can't continue");
                            continue;
                        }
                        System.out.print("\nType the second number\n>> ");
                        y = InputUtils.getNumber();
                        sendData(clientSocket, x, y, c.charAt(0));
                        InputUtils.Receive(clientSocket);
                    } catch (Exception e) {
                        logger.warning(e.toString());
                        continue;
                    }
                } else {
                    logger.warning("Uknown entry, falling back to start");
                }
                action = InputUtils.menuInput();
            }
            clientSocket.close(); //close the socket
        } catch (UnknownHostException e) {
            logger.severe("Error Connecting to host, client exiting\n");
        } catch (IOException e) {
            logger.warning("Something went wrong.\nUnknown host!");
        }
    }

    /**
     * Static method to send data through a socket, in this case send 2 integers and a char
     * @param sock - an open server Socket
     * @param a - first math element
     * @param b - second math element
     * @param c - math operator
     */
    public static void sendData(Socket sock, int a, int b, char c)
    {
        Logger logger = Logger.getLogger("CalculatorClient");
        logger.info("sending Data");
        try {
            DataOutputStream outToServer = new DataOutputStream(sock.getOutputStream());
            String toSend;
            toSend =  String.valueOf(a)+'#'+String.valueOf(b)+'#'+c+'\n';   // properly tokenize elements
            outToServer.writeBytes(toSend);
        } catch (IOException e) {
            logger.severe("Server is unreachable");
            System.exit(1);
        }
    }
}
