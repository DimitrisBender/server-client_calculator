import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * I/O utilities for the client side calculator
 * @author Dimitris
 */

public class InputUtils
{
    private static Logger logger = Logger.getLogger("CalculationLogger");
    /**
     * Get information from the stdin
     * @return user command
     */
    public static String menuInput()
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        boolean flg = false;
        String cmd="";
        System.out.println("Type doMath to perform mathematical calculations.\nOr type exit to exit\n");
        try
        { cmd = reader.readLine();
        } catch (IOException e)
        { logger.warning("I/O error please retry\n");
        }
        return cmd;
    }

    /**
     * Resolve hostname from user
     *
     */
    public static String getHostname()
    {
        BufferedReader hread = new BufferedReader(new InputStreamReader(System.in));
        boolean flg = false;
        String hostname = null;

        try {
            hostname = hread.readLine();
        } catch (IOException e) {
            logger.severe("I/O error please retry\n>>");
        } finally { return hostname; }
    }

    /**
     * Receive a number from the user
     * @return number entry
     */
    public static int getNumber()
    {
        BufferedReader hread = new BufferedReader(new InputStreamReader(System.in));
        int x;
        try {
            x = Integer.parseInt(hread.readLine());
            return x;
        } catch (IOException e) {
            logger.severe("I/O error. exiting");
            return 0;
        }
    }

    /**
     * Receive input data from the server side.
     * @param fromServer - an active server socket
     */
    public static void Receive(Socket fromServer)
    {
        try {
    	    String newSentence = "";
    	    logger.info("Receiving Data:");
            BufferedReader  inFromServer = new BufferedReader(new InputStreamReader(fromServer.getInputStream()));
    	    System.out.println(inFromServer.readLine());
        } catch (IOException e) {
            logger.severe("Error communicating with server");
            System.exit(1);
        }
    }
}
