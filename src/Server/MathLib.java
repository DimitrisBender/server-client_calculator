import java.lang.ArithmeticException;

/**
 * Simple binary math library
 * @author Dimitris
 */
public class MathLib
{
    private int x;
    private int y;


    public MathLib(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // Addition
    public int doAdd() {
        return x+y;
    }

    // Multiplication
    public int doMulti() {
        return x*y;
    }

    // Division
    public float doDivide() {
        if(y==0) {
            throw new ArithmeticException("Division by zero! -.-");
        }
        return x/(float) y;
    }

    // Subtraction
    public int doAb() {
        return x-y;
    }

    // modulo division
    public int modulo() {
        if(y==0) {
            throw new ArithmeticException("Division by zero! -.-");
        }
        return x%y;
    }

}
