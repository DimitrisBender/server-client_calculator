import java.net.Socket;
import java.net.ServerSocket;
import java.io.*;
import java.util.*;
import java.lang.ArithmeticException;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Server side of the calculator
 * @author Dimitris Lygizos
 */
public class ServerMain
{
    public static void main(String[] argv)
    {
        Logger logger = Logger.getLogger("CalculatorServer");
        logger.setLevel(Level.INFO);
        ServerSocket welcomeSocket = null;
        int x = 0, y = 1, port;
        boolean flg = true;
        String c;

        if(argv.length != 1) {
            logger.warning("Syntax error: insufficient command line arguments! Port number expected only");
            return;
        }
        try {
            // get port number
            port = Integer.parseInt(argv[0]);
        } catch (NumberFormatException e) {
            System.out.println("Invalid port format.\nPort is an int from 1024-65635");
            System.out.println("Default port is 64669");
            port = 64669;
        }
        logger.info("port number: " + String.valueOf(port));

        try {
             welcomeSocket = new ServerSocket(port);
        } catch(IOException e) {
            logger.severe(e.getMessage());
            return;
        }
        while(flg == true)
        {
            //accept new connection
            try
            {
                if(welcomeSocket == null) break;
                Socket connectionSocket = welcomeSocket.accept();
                //get the streams
                DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
                BufferedReader  inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                //receive from client
                String clientSentence = inFromClient.readLine();
                while (clientSentence != null) {    // get client command and analyze it's elements
                    StringTokenizer tokenizer = new StringTokenizer(clientSentence, "#");
                    x = Integer.parseInt(tokenizer.nextToken());
                    y = Integer.parseInt(tokenizer.nextToken());
                    c = tokenizer.nextToken();
                    try {
                        // create a human readable format of the result
                        String result = String.valueOf(x) + c + String.valueOf(y) + " = " + String.valueOf(menu(x, y, c)) + "\n";
                        //send result client
                        outToClient.writeBytes(result);
                    } catch(ArithmeticException e) {
                        // Error in calculations
                        String error = "Malformed mathematical expression: ".concat(e.getMessage());
                        logger.warning(error);
                        outToClient.writeBytes(error.concat("\n"));
                    } finally {
                        clientSentence = inFromClient.readLine();
                    }
                }
                // close streams and client socket
                outToClient.flush();
                outToClient.close();
                inFromClient.close();
                connectionSocket.close();
            } catch(IOException e) { continue; }
        }
    }

    /**
     * Call the appropriate math methods to perform the calculation
     * @param x first number
     * @param y second number
     * @param ctl mathematical operator (+,-,*,/)
     * @return result of the math expression, or an warning message
     */
    public static String menu(int x, int y, String ctl) throws ArithmeticException
    {
        MathLib inst = new MathLib(x, y);
        String res;
        if(ctl.equals("+")) {
            res = String.valueOf(inst.doAdd());
        }
        else if(ctl.equals("-")) {
            res = String.valueOf(inst.doAb());
        }
        else if(ctl.equals("*")) {
            res = String.valueOf(inst.doMulti());
        }
        else if(ctl.equals("/")) {
            res = String.valueOf(inst.doDivide());
        }
        else if(ctl.equals("%")) {
            res = String.valueOf(inst.modulo());
        }
        else {
            res = "Invalid operator.\nPlease try again";
        }
        return res;
    }
}
